import { createStore } from 'redux'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const counterReducer = (state = 0, action) => {
    switch(action.type){
        case 'add':
            return state + 1;
        case 'substract':
            return state - 1;
        default:
            return state;

    }
};

const store = createStore(counterReducer);


const App = ({value, onInc, onDec}) => (
    <div className="App">
        <h1>Redux Counter</h1>
        <button onClick={onInc}>Increment</button>
        <button onClick={onDec}>Decrement</button>
        <br></br>
        <span>{value}</span>
    </div>
);


const render = () => {
    ReactDOM.render(
        <App value={store.getState()}
             onInc={() => store.dispatch({type: 'add'})}
             onDec={() => store.dispatch({type: 'substract'})}
        />, document.getElementById('root'));
};

store.subscribe(render);
render();

registerServiceWorker();

